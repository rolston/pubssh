The following is a quick proof of concept to show how to add a user that only allows SSH access via keys. The setup will create the user account grolston, give it sudo access, and then install the public key under the authorized_keys for account. 

Interactive Install
----------------

Run either curl or wget one-liners to install interactively:


**curl**
```bash
curl -fsS https://gitlab.com/rolston/pubssh/raw/master/create-account.sh | sudo bash
```

**wget**
```bash
wget https://gitlab.com/rolston/pubssh/raw/master/create-account.sh -O- | sudo bash
```

AWS CloudFormation EC2 UserData
------------------
In some cases depending on OS, you may not be able to pipe to sh. Therefore to be sure the installation runs, run  one of the following:

**curl**
```bash
 cd /root/
 curl -fsS https://gitlab.com/rolston/pubssh/raw/master/create-account.sh -O
 chmod u+x /root/create-account.sh
 ./create-account.sh

```

**wget**
```bash
 cd /root/
 wget https://gitlab.com/rolston/pubssh/raw/master/create-account.sh
 chmod u+x /root/create-account.sh
 ./create-account.sh
```


